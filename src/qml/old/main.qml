import QtQuick 2.1
import QtQuick.Controls 2.0 as QQC2
import org.kde.kirigami 2.4 as Kirigami
Kirigami.ApplicationWindow {
    id: root
    Today{
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Column{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        spacing: 20
        DayCard{
            id: day1
            anchors.horizontalCenter: parent.horizontalCenter
        }
        DayCard{
            id: day2
            anchors.horizontalCenter: parent.horizontalCenter
            date: "Tue"
            icon: "qrc:/res/01d.png"
            weather: "Sunny"
            temp: "29"
            wind: "12"
            hum: "39"
        }
        DayCard{
            id: day3
            anchors.horizontalCenter: parent.horizontalCenter
            date: "Wed"
            icon: "qrc:/res/01d.png"
            weather: "Sunny"
            temp: "22"
            wind: "14"
            hum: "12"
        }}/*
        DayCard{
            id: day4
            anchors.horizontalCenter: parent.horizontalCenter
            date: "Thu"
            icon: "qrc:/src/res/01d.png"
            weather: "Sunny"
            temp: "22"
            wind: "14"
            hum: "12"
        }
    }*/

    globalDrawer: Kirigami.GlobalDrawer {
        bannerImageSource: "qrc:/res/banner.png"
        bannerVisible: true
        topContent: [
        Row {
            spacing: 5
            Text {
                text: qsTr("Settings")
                font.pointSize: 28
            }
        }]

        Column{
            spacing: 3

            Text{
                text: "Temperature unit"
                font.pointSize: 22
            }

            Row {
                id: tempUnit
                spacing: 2
                QQC2.RadioButton {
                    id: celsius
                    checked: true
                    text: qsTr("Celsius")
                    width: 100
                }
                QQC2.RadioButton {
                    id: fahrenheit
                    checked: false
                    text: qsTr("Fahrenhelt")
                }
            }
            Text{
                text: "Wind volecity unit"
                font.pointSize: 22
            }
            Row {
                id: windUnit
                spacing: 2
                QQC2.RadioButton {
                    id: mph
                    checked: true
                    text: qsTr("mph")
                    width: 100
                }
                QQC2.RadioButton {
                    id: kmph
                    checked: false
                    text: qsTr("kmph")
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
