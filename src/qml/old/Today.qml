import QtQuick 2.0
import QtGraphicalEffects 1.0
Item {
    property url icon: "qrc:/res/02d.png"
    property string weather: "Windy"
    property string temp: "25"
    property string wind: "18"
    property string hum: "73"
    width: 720; height: 256
    Rectangle{
        id: border
        anchors.fill: parent
        color: "#eeeeee"
        Column{
            anchors.fill: parent
            Image {
                source: icon
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Text{
                id: weatherId
                text: weather
                font.pointSize: 50
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Row {
                anchors.horizontalCenter: weatherId.horizontalCenter
                spacing: 20
                Text {
                    text: "🌡" + temp + "°C"
                    font.pointSize: 15
                }
                Text {
                    text: "☴" + wind + "km/h"
                    font.pointSize: 15
                }
                Text {
                    text: "💧" + hum + "%"
                    font.pointSize: 15
                }
            }
            DropShadow{
                anchors.fill: border
                horizontalOffset: 3
                verticalOffset: 3
                radius: 8.0
                samples: 17
                color: "#80000000"
                source: border
            }
        }
    }
}
