import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
Item {
    property string date: "Mon"
    property string weather: "Rainy"
    property url icon: "qrc:/res/11d.png"
    property string temp: "25"
    property string wind: "18"
    property string hum: "73"
    width: 680; height: 130
    Rectangle{
        id: border
        anchors.fill: parent
        color: "#eeeeee"
        RowLayout{
            anchors.fill: parent
            Column{
                id: leftCol
                spacing: 2
                Text{
                    text: date
                    font.pointSize: 20
                }
                Image {
                    source: icon
                }
            }
            Text {
                id: description
                Layout.alignment: Qt.AlignVCenter
                text: weather
                font.pointSize: 30

            }
            Column{
                spacing: 2
                Layout.alignment: Qt.AlignLeft
                Text {
                    text: "🌡" + temp + "°C"
                    font.pointSize: 15
                }
                Text {
                    text: "☴" + wind + "km/h"
                    font.pointSize: 15
                }
                Text {
                    text: "💧" + hum + "%"
                    font.pointSize: 15
                }
            }
        }
    }
    DropShadow{
        anchors.fill: border
        horizontalOffset: 3
        verticalOffset: 3
        radius: 8.0
        samples: 17
        color: "#80000000"
        source: border
    }
}
